<?php

require_once SABRES_PLUGIN_DIR . '/library/fail.php';
require_once SABRES_PLUGIN_DIR . '/_inc/modules/class.backup.db.php';
require_once SABRES_PLUGIN_DIR . '/library/zip.php';

class Backup {
    public function execute($rpc_data) {

      $engine = new SBS_Backup_DB_Engine();
      $engine->init( array(), 'sbs_backup' );
      $engine->run();

      SBS_Zip::archive( ABSPATH, WP_CONTENT_DIR . '/sbs-backup/sbs_backup.zip' );

    }

}
