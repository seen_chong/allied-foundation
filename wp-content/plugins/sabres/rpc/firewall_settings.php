<?php

require_once SABRES_PLUGIN_DIR . '/library/fail.php';
require_once SABRES_PLUGIN_DIR . '/modules/firewall.php';

class Firewall_Settings {
    public function execute($rpc_data) {

      $topic = null;
      $data = null;
      $purge = null;

      if ( !empty( $rpc_data['topic'] ) ) {
          $topic = $rpc_data['topic'];
      }
      if ( !empty( $rpc_data['body'] ) ) {
          $data = urldecode( $rpc_data['body'] );
      }
      if ( !empty( $rpc_data['purge'] ) ) {
          $purge = $rpc_data['purge'];
      }


      if ( !empty( $topic ) && !empty( $data ) ) {
          $firewall = SBS_Firewall::getInstance();

          switch ( $topic ) {
              case 'countries':
                  $firewall->add_countries( json_decode( $data, JSON_OBJECT_AS_ARRAY ) );
                  break;
              case 'cookies':
                  $firewall->add_unique_ids( json_decode( $data, JSON_OBJECT_AS_ARRAY ) );
                  break;
              case 'custom':
                  $firewall->add_custom_range( json_decode( $data, JSON_OBJECT_AS_ARRAY ), $purge );
                  break;
          }
      }
    }

}
