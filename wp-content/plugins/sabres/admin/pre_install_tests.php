<?php


class Pre_Install_Tests {

    public function dispatch($op) {
        //Common Validation
        $result=array();
        if (!defined('SABRES_PLUGIN_DIR'))
            $result['error']="SABRES_PLUGIN_DIR is not defined";
        if (isset($result['error']))
            return $result;
        switch ($op) {
            case 'php-ver':
                return $this->testPHPVersion();
            case 'webserver-type':
                return $this->testWebServer();
            case 'openssl':
                return $this->testOpenSSL();
            case 'vendor-download' :
                return $this->testVendorDownload();
            case 'vendor-extract':
                return $this->testVendorExtract();
            case 'finalize':
                return $this->finalize();
            default:
                return $this->defaultCase($op);
        }
    }
    public function finalize() {
        $result=$this->dispatch('php-ver');
        if (isset($result['error']))
            return array('success'=>false);
        $result=$this->dispatch('webserver-type');
        if (isset($result['error']))
            return array('success'=>false);
        $result=$this->dispatch('openssl');
        if (isset($result['error']))
            return array('success'=>false);
        $downloadVendorsFile=SABRES_PLUGIN_DIR.'/downloadVendors';
        if (file_exists($downloadVendorsFile))
            return array('success'=>false);
        SbrSettings::instance()->preInstall="False";
        return array('success'=>true);
    }


    public function testPHPVersion() {
        $result=array();
        $result['title']="Checking PHP version (required 5.4 and above)";
        if ( version_compare( PHP_VERSION, '5.4', '<' ) )
            $result['error']="Failed your version is : ".phpversion();
        else
            $result['message']="Passed your version is : ".phpversion();
        return $result;

    }
    public function testOpenSSL() {
        $result=array();
        $result['title']="Checking open SSL Extension";
        if ( !extension_loaded('openssl') )
            $result['error']="Failed open SSL extension is not loaded";
        else
            $result['message']="Passed open SSL extension is loaded";
        return $result;

    }
    public function testWebServer() {
        $result=array();
        $result['title']="Checking hosting web server";
        $phpInfoJson=strtoupper(json_encode(SbrUtils::phpinfo_array(true)));
        $apacheOccurs=substr_count($phpInfoJson, strtoupper('apache'));
        $iisOccurs=substr_count($phpInfoJson, strtoupper('iis'));
        $nginxOccurs=substr_count($phpInfoJson, strtoupper('nginx'));
        if ($apacheOccurs==0 && $iisOccurs==0 && $nginxOccurs) {
            $result['message']="Uknown web server";
        }
        else {
            $webserver=null;
            $max=max($apacheOccurs,$iisOccurs,$nginxOccurs);
            if ($max==$apacheOccurs)
                $webserver='Apache';
            elseif ($max==$nginxOccurs)
                $webserver='Nginx';
            else
                $webserver='IIS';
            $result['message']=$webserver.' web server detected';
        }
        return $result;

    }

    private function optionNameVendorsDownloaded() {
        return 'Sabres_VendorsDownloaded';
    }

    private function downloadVendorArchive(&$result) {
        if (!defined('SBS_MAIN_PLUGIN_FILE')) {
            $result['error']="SBS_MAIN_PLUGIN_FILE is not defined";
            return;


        }
        $pluginData=get_plugin_data(SBS_MAIN_PLUGIN_FILE);
        if (!isset($pluginData)) {
            $result['error']="Could not find plugin data";
            return;
        }
        if (!isset($pluginData)) {
            $result['error']="Could not find plugin data";
            return;
        }
        if (!isset($pluginData['Version'])) {
            $result['error']="Could not find plugin version";
            return;
        }
        $downloadPath=SbrUtils::trgx('plugin_download_url');
        $vendorsZip=$downloadPath."/vendors-".$pluginData['Version'].".zip";
        require_once ABSPATH . 'wp-admin/includes/file.php';
        $download=download_url($vendorsZip);
        if (is_wp_error($download)) {
            $result['error']="Failed to download ".$vendorsZip.". ".$download->get_error_message().' '.$download->get_error_data();
            return;
        }

        return array('zipURL'=>$vendorsZip,'downloadPath'=>$download);
    }


    public function testVendorDownload() {
        $result=array();
        $result['title']="Checking vendor libs downloaded successfully";


        $vendorsDownloaded=get_option($this->optionNameVendorsDownloaded(),null);
        if (!isset($vendorsDownloaded)) {
            $downloadData=$this->downloadVendorArchive($result);
            if (isset($downloadData)) {
                add_option($this->optionNameVendorsDownloaded(),$downloadData['downloadPath']);
                $result['message']='Successfully downloaded vendors zip: '.$downloadData['zipURL'];
            }
        }
        else {
            $result['message']="Vendor libs already downloaded successfully";
        }
        return $result;
    }

    public function testVendorExtract() {
        $result=array();
        $result['title']="Extracting vendor libs zip file";
        $vendorsDownloadFile=get_option($this->optionNameVendorsDownloaded(),null);
        if (!isset($vendorsDownloadFile))
            $result['error']='Venodr libs file was not downloaded';
        else  {
            global $wp_filesystem;
            // Initialize the WP filesystem, no more using 'file-put-contents' function
            if (empty($wp_filesystem)) {
                require_once (ABSPATH . '/wp-admin/includes/file.php');
                WP_Filesystem();
            }
            $destination=SABRES_PLUGIN_DIR.'/library';
            $unzipResult=unzip_file($vendorsDownloadFile,$destination);
            if (is_wp_error($unzipResult)) {
                $result['error']="Failed to extract vendor libs. ".$unzipResult->get_error_message().' '.$unzipResult->get_error_data();
            }
            else {
                unlink($vendorsDownloadFile);
                $downloadVendorsFile=SABRES_PLUGIN_DIR.'/downloadVendors';
                if (file_exists($downloadVendorsFile))
                    unlink($downloadVendorsFile);
                delete_option($this->optionNameVendorsDownloaded());
                $result['message']="Successfully extracted vendor libs";
            }
        }
        return $result;
    }


    public function defaultCase($op) {
        $result=array();
        header('HTTP/1.1 500 Internal Server Error');
        $result['error']='Invalid op '.$op;
        return $result;

    }

}