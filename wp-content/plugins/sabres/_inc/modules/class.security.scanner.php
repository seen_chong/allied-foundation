<?php

// Exit if accessed directly
if (!defined('SBS_PATH')) {
  exit;
}

/*
 * Copyright 2016 Sabres Security Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


if (!class_exists('SBS_Security_Scanner')) {

  require_once 'class.security.scanner.engine.php';

  /**
   * The Sabres Security Scanner Class
   *
   * @author Jonnie - Sabres Security Team
   * @package Sabres_Security_Plugin
   * @since 1.0.0
   */
  final class SBS_Security_Scanner extends SBS_Singleton {

    private $settings;
    private $server;
    private $current_scan;
    protected static $instance;

    protected function __construct() {
      $this->server = SBS_Server::getInstance();
    }

    public function init($settings = null) {
      $this->settings = (object) array_change_key_case($settings, CASE_LOWER);
    }

    public function run($fix = false, $guid = 0) {
      // Disable time limit
      @ini_set('max_execution_time', '0');
      @set_time_limit(0);

      // Disable memory limit
      @ini_set('memory_limit', '-1');

      if ($fix) {
        $engine = new SBS_Security_Scanner_Engine(0);
        return $engine->fix_issue($guid);
      }
      
      $this->stop_scans();

      $this->current_scan = $this->add_scan();
      $this->current_scan->items = array();

      $engine = new SBS_Security_Scanner_Engine($this->current_scan);

      $engine->register_event_callback('status', function( $status ) {
        $this->set_status($status);

        switch ($status) {
          case 'ended':
//                        $this->server->call( 'job-complete', null, array(
//                                'jobID' => $this->current_scan->ID,
//                                'module' => 'security',
//                                'resultText' => json_encode( $this->current_scan->items ),
//                                'resultType' => 1
//                            )
//                        );
            break;
        }
      });

      $engine->register_event_callback('info', function( $desc ) {
        $this->add_scan_item('info', $desc);
      });

      $engine->register_event_callback('error', function( $desc ) {
        $this->add_scan_item('error', $desc);
      });

      $engine->register_event_callback('issue', function( $risk_level, $desc, $item_sub_type, $file_name, $line_number, $content ) {
        $this->add_scan_item('issue', $desc, $item_sub_type, $risk_level, $file_name, $line_number, $content);
      });

      $engine->start();
    }

    public function get_current_scan() {
      return $this->current_scan;
    }

    private function stop_scans() {
      global $wpdb;

      $table_name = $wpdb->prefix . 'sbs_scan_items';
      $sql = "DELETE FROM $table_name";
      $wpdb->query($sql);

      $table_name = $wpdb->prefix . 'sbs_scans';
      $sql = "DELETE FROM $table_name";
      $wpdb->query($sql);

      return true;
    }

    private function set_status($status) {
      if (isset($this->current_scan)) {
        global $wpdb;
        $table_name = $wpdb->prefix . 'sbs_scans';

        $sql = "UPDATE $table_name SET $table_name.status = '%s', $table_name.updated_at = now() WHERE ID = %d";

        return $wpdb->query($wpdb->prepare($sql, $status, $this->current_scan->ID));
      }
    }

    public function get_scans_by_status($status) {
      global $wpdb;

      $table_name = $wpdb->prefix . 'sbs_scans';

      $query = "SELECT * FROM $table_name WHERE $table_name.status = '$status'";

      return $wpdb->get_results($query, OBJECT);
    }

    private function get_scan($id) {
      global $wpdb;

      $table_name = $wpdb->prefix . 'sbs_scans';

      $query = "SELECT * FROM $table_name WHERE $table_name.ID = $id";

      return $wpdb->get_row($query, OBJECT);
    }

    private function get_scan_item($id) {
      global $wpdb;

      $table_name = $wpdb->prefix . 'sbs_scan_items';

      $query = "SELECT * FROM $table_name WHERE $table_name.ID = $id";

      return $wpdb->get_row($query, OBJECT);
    }

    private function add_scan() {
      global $wpdb;

      $table_name = $wpdb->prefix . 'sbs_scans';

      $sql = "INSERT INTO $table_name ( $table_name.scan_type, $table_name.status) VALUES (%s, %s)";

      if ($wpdb->query($wpdb->prepare($sql, 'security', 'idle'))) {
        return $this->get_scan($wpdb->insert_id);
      }
    }

    private function add_scan_item($type, $desc, $item_sub_type = "", $risk_level = null, $file_name = null, $line_number = null, $content = null) {
      if (isset($this->current_scan)) {
        global $wpdb;

        $table_name = $wpdb->prefix . 'sbs_scan_items';
        $unique_id = md5($file_name . $line_number . $content);

        $sql = "INSERT INTO $table_name ( $table_name.parent_id, $table_name.item_type, $table_name.item_desc, $table_name.item_sub_type, $table_name.risk_level, $table_name.unique_id) VALUES (%d, %s, %s, %s, %d, %s)";

        $result = $wpdb->query($wpdb->prepare($sql, $this->current_scan->ID, $type, $desc, $item_sub_type, $risk_level, $unique_id));
        if ($result) {
          $item = $this->get_scan_item($wpdb->insert_id);

          $this->current_scan->items[] = $item;

          return $item;
        } else {
          // print_r($wpdb->last_error); //  what should we do on error? 
        }
      }
    }

  }

}