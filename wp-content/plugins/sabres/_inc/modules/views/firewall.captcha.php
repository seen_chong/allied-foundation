<html>
   <head>
       <link href="<?php echo SBS_PLUGIN_URL . '/admin/styles/bootstrap.css'; ?>" rel="stylesheet">
       <link href="<?php echo SBS_PLUGIN_URL . '/admin/styles/style.css'; ?>" rel="stylesheet">
   </head>
   <body>
      <div class="sbr-captcha">
          <div class="sbr-captcha-panel">
              <div class="sbr-captcha-title">
                  A minor step to keep our website safe
              </div>
              <div class="sbr-captcha-container">
                  <div class="sbr-captcha-img"><img src="<?php echo $captcha_data ?>"/></div>
                  <div class="sbr-captcha-arrow"></div>
              </div>

              <div class="sbr-captcha-controls">
                  <div class="sbr-captcha-label">
                      Type the word above
                  </div>
                  <div class="sbr-captcha-inputs">
                      <form method="post">
                          <input type="text" name="sbs_firewall_captcha_phrase" maxlength="6" class="sbr-captcha-input"/>
                          <button class="sbr-captcha-refresh-button" name="sbs_firewall_captcha_refresh"></button>
                          <button class="sbr-captcha-submit-button">Submit</button>
                      </form>
                  </div>
              </div>
          </div>
          <div class="sbr-captcha-footer">
              This website is protected by Sabres Security
          </div>
      </div>
      <?php Sabres::write_firewall_action_script( 'C' ); ?>
   </body>
</html>