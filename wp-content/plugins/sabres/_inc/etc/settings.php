<?php

if (!defined('SABRES_RPC')) {
    $plugin_current_version = get_option( 'sbs_version_number' );
    $plugin_remote_path = SbrUtils::t( 'plugin_download_url' );
    $plugin_slug = 'sabres/sabress.php';
    require_once (SABRES_PLUGIN_DIR . '/library/autoupdate.php');
    new Sabres_WP_AutoUpdate ( $plugin_current_version, $plugin_remote_path, $plugin_slug, null, null );
}