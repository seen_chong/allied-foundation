<?php

class Sabres_Config {
  private static $values=array(
    'ABSPATH'=>'/home/alliedfoundation/public_html/',
    'SABRES_PLUGIN_DIR'=>'/home/alliedfoundation/public_html/wp-content/plugins/sabres/',
    'SABRES_PLUGIN_BASE_NAME'=>'sabres/sabress.php',
    'SBS_PLUGIN_URL'=>'http://alliedfoundation.org/wp-content/plugins/sabres',
    'SBS_MAIN_PLUGIN_FILE'=>'/home/alliedfoundation/public_html/wp-content/plugins/sabres/sabress.php',
    'WP_CONTENT_DIR'=>'/home/alliedfoundation/public_html/wp-content'
  );

  public static function get($key) {
    return self::$values[$key];
  }

  public static function any_value_changed() {
    $constants=get_defined_constants();
    foreach (self::$values as $key => $value) {
      if (rtrim($constants[$key], '/')!=rtrim($value, '/'))
        return true;
    }
    return false;
  }
  public static function is_plugin_url_changed() {
    return str_replace('https://','http://',rtrim(self::$values['SBS_PLUGIN_URL'],'/'))!=str_replace('https://','http://',rtrim(SBS_PLUGIN_URL,'/'));
  }

  public static function reset() {
    $constants=get_defined_constants();
    foreach (self::$values as $key => $value) {
      self::$values[$key]=$constants[$key];
    }
  }

  public static function define_all() {
    foreach (self::$values as $key => $value) {
      define($key,$value);
    }
  }

  public static function print_config() {
    return print_r(self::$values,true);
  }

  public static function get_version() {
    return 2;
  }
}
