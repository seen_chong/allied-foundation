<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}
class Sabres_User_Capabilities {

  protected static $privileged_caps = [
      'create_sites',
      'delete_sites',
      'manage_network',
      'manage_sites',
      'manage_network_users',
      'manage_network_plugins',
      'manage_network_themes',
      'manage_network_options',
      'upload_plugins',
      'upload_themes',
      'activate_plugins',
      'create_users',
      'delete_plugins',
      'delete_themes',
      'delete_users',
      'edit_files',
      'edit_plugins',
      'edit_theme_options',
      'edit_themes',
      'edit_users',
      'export',
      'import',
      'install_plugins',
      'install_themes',
      'list_users',
      'manage_options',
      'promote_users',
      'remove_users',
      'switch_themes',
      'update_core',
      'update_plugins',
      'update_themes',
      'edit_dashboard',
      'customize',
      'delete_site'
  ];

  public static function is_user_admin($user) {
    if (empty($user) || !($user instanceof WP_User) || $user->ID==0  ) {
      return false;
    }
    if (is_super_admin($user))
      return true;
    return count(self::intersect_admin_capabilities(array_keys($user->allcaps )))>0;
  }

  public static function intersect_admin_capabilities($capabilities) {
    return array_intersect( self::$privileged_caps, $capabilities);

  }

}


?>
