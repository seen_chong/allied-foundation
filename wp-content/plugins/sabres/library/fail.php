<?php

class SBS_Fail {

  public static function bye($message,$logData=null,$includeBacktrace=true,$code=null) {
    if (!is_null($code)) {
      if ( !headers_sent() ) {
          foreach ( headers_list() as $header ) {
              @header_remove( $header );
          }
      }      
      @header( 'X-PHP-Response-Code', true, $code );

    }
    $result=array();
    $result['error']=$message;
    if (isset($logData))
      $result['data']=$logData;
    if ($includeBacktrace) {
      $result['backtrace']=debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
    }
    die(json_encode($result));

  }

  public static function byeArr($args) {
    $message=@$args['message'];
    $logData=@$args['logData'];
    $includeBacktrace=@$args['includeBacktrace'];
    $code=@$args['code'];
    self::bye($message,$logData,$includeBacktrace,$code);

  }

}
