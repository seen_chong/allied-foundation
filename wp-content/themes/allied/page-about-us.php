<?php get_header(); ?>

<section id="page-header">
                <h1 class="color-orange"><?php the_title(); ?></h1>
            </section>

            <section id="board-intro">
                <div id="board-wrap" class="board-figures">

                    <div class="board-members">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/garymirkin.jpeg">
                        <h2>Gary Mirkin MD<h2>
                    </div>
                                                <div class="board-members">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/fierstein.jpg">
                        <h2>Kerry Fierstein MD<h2>
                    </div>
                    <div class="board-members">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/jennifershaer.jpeg">
                        <h2>Jennifer Shaer MD<h2>
                    </div>
                    <div class="board-members">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/etanwalls.jpeg">
                        <h2>Etan Walls<h2>
                    </div>
                    <div class="board-members">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/scott.png">
                        <h2>Scott Svitek MD<h2>
                    </div>
                        

                    <div class="other_members">
                    </div>
                    <div class="board-members">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/goldberg.jpg">
                        <h2>
                            <a href="mailto:kgg@alliedfoundation.org">Kerry Gillick-Goldberg</a>
                            <h2>
                            <p>Executive Director</p>
                    </div>
                    <div class="board-members">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/brianne.jpg">
                        <h2>Brianne Chidichimo<h2>
                            <p>Secretary</p>
                    </div>

                </div>

                <div id="board-bio">
                    <div id="board-wrap-80">
<!--
                        <p>Allied Foundation is a not-for-profit organization founded by Allied Physicians Group.</p>
                        <p>The mission of Allied Foundation is to improve the health and well-being of people in the community. Allied Foundation uses a combination of fundraising and program implementation to achieve its mission.<p>
-->     
                        <?php the_field('description'); ?>
                    </div>
                </div>
                
            </section>

            <section id="visit-allied">
                <a href="http://alliedphysiciansgroup.com" target="blank">
                	<img src="<?php echo get_template_directory_uri(); ?>/img/allied-visit-site.jpg">
                </a>
            </section>



<?//php get_sidebar(); ?>

<?php get_footer(); ?>
