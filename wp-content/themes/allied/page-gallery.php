<?php get_header(); ?>

<section id="page-header">
                <h1 class="color-orange"><?php the_title(); ?></h1>
            </section>

            <section id="board-intro">
                <div class="foundation_gallery">
                    <?php echo do_shortcode('[envira-gallery id="135"]'); ?>

                    
                </div>
                
            </section>

            <section id="visit-allied">
                <a href="http://alliedphysiciansgroup.com" target="blank">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/allied-visit-site.jpg">
                </a>
            </section>

            

<?php get_footer(); ?>
