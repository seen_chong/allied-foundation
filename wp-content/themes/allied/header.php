<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

	</head>
	<body <?php body_class(); ?>>

		<!-- wrapper -->
		<div id="body-wrap">

			<!-- header -->
			<header role="banner">
                <nav id="nav-bar" role="navigation">

                	<!-- logo -->
                    <ul id="nav-logo">
                        <li>
                        	<a href="<?php echo home_url(); ?>">
                        		<img src="<?php echo get_template_directory_uri(); ?>/img/allied-logo.png">
                        	</a>
                        </li>
                    </ul>
                    <!-- /logo -->

                    <!-- nav -->
                    <?//php html5blank_nav(); ?>

                    <ul id="drop-nav">
                    	<li><a href="/about-us">About Us</a></li>

					 	<li><a href="#">Projects</a>
					    	<ul>
					      		<li class="has-dropdown"><a href="/breastfeeding">Newborn Support</a>
                                    <ul class="sub-dropdown">
                                        <li><a href="/breastfeeding/#support_list">Diaper Bank</a></li>
                                        <li><a href="/breastfeeding/#support_list">Support Line</a></li>
                                        <li><a href="/breastfeeding/#support_list">Lactation Consultants</a></li>
                                        <li><a href="/breastfeeding/#support_list">Breastfeeding Medicine</a></li>
                                        <li><a href="/breastfeeding/#support_list">Extra Breast Milk?</a></li>
                                    </ul>
                                </li>

					      		<li><a href="/education">Community Education</a></li>
					      		<li><a href="/literacy">Early Childhood Literacy</a></li>
                                <li><a href="/service">Community Service</a></li>
					    	</ul>
					  	</li>

					  	<li><a href="/media">Media</a></li>
                        <li><a href="/gallery">Gallery</a></li>
                        <li><a href="/contact-us">Contact Us</a></li>
                        <li>
                            <button>
                                <a href="/donate">Donate</a>
                            </button>
                        </li>


					</ul>
                </nav>
                <!-- /nav -->

            </header>

            

            <!-- /header -->

