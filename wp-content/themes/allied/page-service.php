<?php get_header(); ?>

<div class="main-banner-image "style="background-image:url(<?php the_field('main_image'); ?>); height:750px; background-repeat:no-repeat; background-size:cover; background-position:center center;"
    ''></div>


<section id="page-header" class="background-orange">
                <h1><?php the_title(); ?></h1>
</section>

<section id="project-intro">
                <div id="project-blurb">
                    <div id="project-wrap-80">
                        <?php the_field('content'); ?>
                    </div>
                </div>
            </section>

<!--             <section id="projects" class="background-grey">
                <h1>Check Out Our Other Campaigns</h1>
            <//?php
                $args = array(
                'post_type' => 'projects'
                );
                $products = new WP_Query( $args );
                    if( $products->have_posts() ) {
                    while( $products->have_posts() ) {
                $products->the_post();
            ?>

                <div id="project-breasts" class="project-float project-width">
                    <a href="<?php the_field('link'); ?>">
                        <img src="<?php the_field('image'); ?>">
                        <h2><?php the_field('title'); ?></h2>
                    </a>
                    <p><?php the_field('short_summary'); ?></p>
                </div>

        <//?php
            }
                }
            else {
            echo 'No Campaigns Found';
            }
        ?>
        <??>
            </section> -->

            <section id="contact">
                <h5>Need Community Service Hours? Looking to do a Mitzvah Project?<br>

Please fill out the form below and we’ll reach out to you to discuss!</h5>
                <div class="contact-form ">
                    <?php
                    echo do_shortcode('[contact-form-7 id="15" title="Contact Us"]');
                    ?>
                </div>
            </section>

<!--
            <section id="visit-allied">
                <a href="http://alliedphysiciansgroup.com" target="blank">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/allied-visit-site.jpg">
                </a>
            </section>
-->

            


<?//php get_sidebar(); ?>

<?php get_footer(); ?>
