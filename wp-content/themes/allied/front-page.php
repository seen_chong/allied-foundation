<?php get_header(); ?>

	<section id="slider" style="background-image:url(http://alliedfoundation.org/wp-content/uploads/2015/11/image001-11.jpg); ?>">
                <!-- Background image inserted here through css -->
    </section>

                    <div class="contain40">  
                        <a>
                        <h1>Welcome to Allied Foundation</h1></a>
                        <?php the_field('welcome_content'); ?>

                    </div>

            <section id="projects">

            <?php
                $args = array(
                'post_type' => 'projects'
                );
                $products = new WP_Query( $args );
                    if( $products->have_posts() ) {
                    while( $products->have_posts() ) {
                $products->the_post();
            ?>

                <div id="project-breasts" class="project-float project-width">
                    <a href="<?php the_field('link'); ?>">
                        <div class="project_bg" style="background-image:url(<?php the_field('image'); ?>)">

                        </div>
                        <h1><?php the_field('title'); ?></h1>
                    </a>
                    <p><?php the_field('short_summary'); ?></p>

                    <button><a href="<?php the_field('link'); ?>">Read More</a></button>
                </div>

        <?php
            }
                }
            else {
            echo 'No Projects Found';
            }
        ?>
        <??>
            </section>

            <div id="double-section">
                <section id="calendar">
                <div class="wrapper98">
                    <h1>Upcoming Events</h1>
                                    
                    <div class="contain100">    

                    <img src="https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F39552241%2F53119374868%2F1%2Foriginal.jpg?w=800&auto=compress&rect=0%2C224%2C2470%2C1235&s=e54115152cd77bcaac7ede7e22bd2741">
                        <p>Organizer of Allied Foundation's Spring Lecture Series Hosts Cornell Cooperative- Parent University</p>
                        <p>Sign up here: <a href="https://www.eventbrite.com/e/allied-foundations-spring-lecture-series-hosts-cornell-cooperative-parent-university-tickets-42131280795" target="_blank" style="color:#e65325;text-decoration:underline;">Event Signup</a></p>
                        <p><strong>Read More about parenting workshop in the <a href="https://www.newsday.com/lifestyle/family/parenting-discipline-allied-physicians-sleep-1.18360843" target="_blank" style="color:#e65325;text-decoration:underline;">New York Newsday</a></strong></p>
                        <h3>Allied Physicians Group to host new parenting lecture series</h3>
                        <img src="https://cdn.newsday.com/polopoly_fs/1.18360842.1525281347!/httpImage/image.jpeg_gen/derivatives/landscape_768/image.jpeg" alt="The Allied Physicians Group's parenting sessions in Melville" style="width:90%;height:auto;max-width:600px;">
                        <br><br>
                        <img src="http://alliedfoundation.org/wp-content/uploads/2018/06/saturday.jpg">
                        <img src="http://alliedfoundation.org/wp-content/uploads/2018/06/image001-1.jpg">
                        
                    <?php echo do_shortcode("[my_calendar]"); ?>
<!--
                                        <p>With your support -as well as the efforts of the Long Island Children's Choir - we hope to collect #BackpacksAndSchoolSupplies for a minimum of 500 students in need. </p>
                    <p>Please bring #NewItemsOnly to your Allied Physicians Group doctor and drop them in their collection crate.</p>
                    <h6>Drop off locations include - but are not limited to: </h6>
-->
<!--
                        <ul class="locations">
                            
                            <li>
                                <a href="http://pediatricmed.alliedphysiciansgroup.com/map-locations/central-long-island-pediatrics" target="_blank">
                                Central Long Island Pediatrics</a>
                            </li>
                            <li>
                                <a href="http://pediatricmed.alliedphysiciansgroup.com/map-locations/nassau-pediatric-associates/" target="_blank">
                                Nassau Pediatric Associates</a> 
                            </li>
                            <li>
                                <a href="http://pediatricmed.alliedphysiciansgroup.com/map-locations/garden-city-pediatric-associates/" target="_blank">
                                Garden City Pediatric Associates</a>
                                
                            </li>
                            <li>
                                <a href="http://pediatricmed.alliedphysiciansgroup.com/map-locations/mds-4-kids-medical/" target="_blank">
                                Mds4kids</a>
                                
                            </li>
                            <li>
                                <a href="http://pediatricmed.alliedphysiciansgroup.com/map-locations/pro-kids/" target="_blank">
                                ProKids Pediatrics</a>
                                
                            </li>
                            <li>
                                <a href="http://pediatricmed.alliedphysiciansgroup.com/map-locations/beach-pediatrics/" target="_blank">
                                Beach Pediatrics</a>
                                
                            </li>
                            <li>
                                <a href="http://pediatricmed.alliedphysiciansgroup.com/map-locations/children-medical-group-bayside/" target="_blank">
                                Children's Medical Group- Plainview & Bayside</a>
                                
                            </li>
                            <li>
                                <a href="http://pediatricmed.alliedphysiciansgroup.com/map-locations/valley-stream-pediatrics/" target="_blank">
                                Valley Stream Pediatrics</a>
                            <li>
                                <a href="http://pediatricmed.alliedphysiciansgroup.com/map-locations/commack-pediatric-associates/" target="_blank">
                                Commack Pediatric Associates</a>
                                
                            </li>
                            <li>
                                <a href="http://pediatricmed.alliedphysiciansgroup.com/map-locations/jason-hitner-md/" target="_blank">
                                Dr. Hitner - Newborn Care & Ear Piercing Services</a>
                            </li>
                            <li>
                                <a href="http://pediatricmed.alliedphysiciansgroup.com/map-locations/md-kid-care/" target="_blank">
                                Dr. Monica Melamedoff- MD Kid Care</a>
                                
                            </li>
                            <li>
                                <a href="http://pediatricmed.alliedphysiciansgroup.com/map-locations/peconic-pediatrics-riverhead/" target="_blank">
                                Peconic Pediatric Medicine</a>
                                
                            </li>
                            <li>
                                <a href="http://pediatricmed.alliedphysiciansgroup.com/map-locations/peds-first-pediatrics/" target="_blank">
                                Peds First Pediatrics - Medford</a>
                                
                            </li>
                            <li>
                                <a href="http://pediatricmed.alliedphysiciansgroup.com/map-locations/pediatric-health-associates/" target="_blank">
                                Pediatric Health Associates</a>
                                
                            </li>
                                
                        </ul>
-->
                    </div>   

                    </div>
                </section>
                
                
                <section id="news">
                <div class="wrapper80">
                    <h1> In The News</h1> 
                        
            <?php
                $args = array(
                'post_type' => 'articles'
                );
                $products = new WP_Query( $args );
                    if( $products->have_posts() ) {
                    while( $products->have_posts() ) {
                $products->the_post();
            ?>
                    <div id="news-blog-2" class="news-blog-side" >
                        <a href="<?php the_field('link'); ?>" target="blank">
                            <img src="<?php the_field('image'); ?>">
                            <h2><?php the_field('headline'); ?></h2>
                        </a>
                        <h3><?php the_field('publication'); ?></h3>
                        <p><?php the_field('excerpt'); ?></p>

                    </div>
                    
        <?php
            }
                }
            else {
            echo 'No Articles Found';
            }
        ?>
        <??>
                        </div>
                </section>


            </div>

            <section id="mission">
                <p><?php the_field('mission_text'); ?></p>
            </section>
            
            <section id="sponsor">
                <div id="sponsor-text">
                    <h1>We wish to thank our sponsors</h1>
                </div>
                <div id="sponsor-logos">
                	<div class="mobile_no">
                	<?php
                    echo do_shortcode('[gs_logo]');
                    ?>
                </div>
                	<ul class="mobile_show" style="display:none;">

                	<?php
		                $args = array(
		                'post_type' => 'sponsors'
		                );
		                $products = new WP_Query( $args );
		                    if( $products->have_posts() ) {
		                    while( $products->have_posts() ) {
		                $products->the_post();
		            ?>


                        <li><img src="<?php the_field('logo'); ?>"></li>
                    
		        <?php
		            }
		                }
		            else {
		            echo 'No Projects Found';
		            }
		        ?>
		        <??>
		        </ul>
                </div>
            </section>

            <section id="contact">
                <h1><a href="/contact-us">Get In Touch</a></h1>
                <div class="contact-form ">
                    <?php
                    echo do_shortcode('[contact-form-7 id="15" title="Contact Us"]');
                    ?>
                </div>
            </section>

<?//php get_sidebar(); ?>

<?php get_footer(); ?>
