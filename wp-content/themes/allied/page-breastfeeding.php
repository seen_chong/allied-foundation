<?php get_header(); ?>

<div class="main-banner-image "style="background-image:url(<?php the_field('breastfeeding_image'); ?>); height:950px; background-repeat:no-repeat; background-size:cover; background-position: center;"
    ''></div>

<!-- <section id="slider-projects" >
                Background image inserted here through css
</section> -->

<section id="page-header" class="background-orange">
                <h1><?php the_title(); ?></h1>
</section>

<section id="project-intro">
                <div id="project-blurb">
                    <div id="project-wrap-80">
                        <?php the_field('content'); ?>
                    </div>
                </div>


                    <div class="breastfeed_list_wrapper">
                        <ul class="breastfeed_list" id="support_list">
                            <li id="support">
                                <button>TOLL-FREE SUPPORT LINE: <a href="tel:866-621-2769">1.866.621.2769</a><br> <br> <i>click to view</i></button>
                            </li>
                            <li id="consultant">
                                <button>ALLIED LACTATION CONSULTANTS AVAILABLE <br> <br> <i>click to view</button>
                            </li>
                            <li id="medicine">
                                <button>BREASTFEEDING MEDICINE PRACTICE <br> <br> <i>click to view</button>
                            </li>
                            <li id="milk">
                                <button>GOT EXTRA BREAST MILK? <br><br> <i>click to view</i></button>
                            </li>
                            <li id="diapers">
                                <button>GOT DIAPERS? <br><br> <i>click to view</i></button>
                            </li>
                        </ul>

                        <div class="reveal_content" id="support_content" style="display:none;">
                            <h3>TOLL-FREE SUPPORT LINE: </h3>
                            <?php the_field('toll_free_support'); ?>

                        </div>

                        <div class="reveal_content" id="consultant_content" style="display:none;">
                            <h3>ALLIED LACTATION CONSULTANTS AVAILABLE: </h3>
                            <?php the_field('consultants_available'); ?>

                        </div>

                        <div class="reveal_content" id="medicine_content" style="display:none;">
                            <h3>BREASTFEEDING MEDICINE PRACTICE:</h3>
                            <?php the_field('medicine_practice'); ?>

                        </div>

                        <div class="reveal_content" id="milk_content" style="display:none;">
                            <h3>GOT EXTRA BREAST MILK? </h3>
                            <?php the_field('breast_milk'); ?>

                        </div>

                        <div class="reveal_content" id="diapers_content" style="display:none;">
                            <h3>GOT DIAPERS? </h3>
                            <?php the_field('diapers'); ?>

                        </div>
                    </div>
            </section>

            <style type="text/css">
            .breastfeed_list ul li button i {
                margin-top: 10px;
                font-size: 12px;
            }
                .reveal_content {
                    background-color: #F1F6F9;
                    width: 80%;
                    margin: 40px auto;
                    padding: 15px 30px;
                }
                .reveal_content ul li {
                    width: 90%;
                    margin: 5px auto;
                    list-style-type: disc;
                }

            </style>

            <script type="text/javascript">
                $(document).ready(function(){

                    $("#support").click(function () {
                        $(".reveal_content").hide();
                        $("#support_content").show();
                    });

                    $("#consultant").click(function () {
                        $(".reveal_content").hide();
                        $("#consultant_content").show();
                    });

                    $("#medicine").click(function () {
                        $(".reveal_content").hide();
                        $("#medicine_content").show();
                    });

                    $("#milk").click(function () {
                        $(".reveal_content").hide();
                        $("#milk_content").show();
                    });

                    $("#diapers").click(function () {
                        $(".reveal_content").hide();
                        $("#diapers_content").show();
                    });







    });

            </script>

<!--             <section id="projects" class="background-grey">
                <h1>Check Out Our Other Campaigns</h1>
            <//?php
                $args = array(
                'post_type' => 'projects'
                );
                $products = new WP_Query( $args );
                    if( $products->have_posts() ) {
                    while( $products->have_posts() ) {
                $products->the_post();
            ?>

                <div id="project-breasts" class="project-float project-width">
                    <a href="<?php the_field('link'); ?>">
                        <img src="<?php the_field('image'); ?>">
                        <h2><?php the_field('title'); ?></h2>
                    </a>
                    <p><?php the_field('short_summary'); ?></p>
                </div>

        <//?php
            }
                }
            else {
            echo 'No Campaigns Found';
            }
        ?>
        <??>
            </section> -->

            <section id="visit-allied">
                <a href="http://alliedphysiciansgroup.com" target="blank">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/allied-visit-site.jpg">
                </a>
            </section>

            


<?//php get_sidebar(); ?>

<?php get_footer(); ?>
