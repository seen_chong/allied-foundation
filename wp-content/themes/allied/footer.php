			<div class="clear-footer">
                <!-- semantics -->
            </div>

			<!-- footer -->
			<footer>
                <div id="footer-widget-1" class="footer-float footer-width">
                    <span id="footer-explore">
                        <h1>Explore</h1>
                    </span>
                    <ul id="footer-explore-links">
                        <li><a href="/about-us">About Us</a></li>
                        <li><a href="/breastfeeding">Breastfeeding</a></li>
                        <li><a href="/education">Education</a></li>
                        <li><a href="/literacy">Literacy</a></li>
                        <li><a href="/media">Media</a></li>
                    </ul>
                </div>

                <div id="footer-widget-2" class="footer-float footer-width">
                    <span id="footer-location">
                        <h1>Location</h1>
                    </span>
                    <span id="footer-location-text">
                        <p>Allied Foundation <br />
                            3 Huntington Quadrangle Suite 105s <br />
                            Melville, NY 11747 <br />
                            <a href="tel:631-386-4143">(631) 386-4143</a>
                        </p>    
                    </span>
                </div>

                <div id="footer-widget-3" class="footer-float footer-width">
                    <span id="footer-contact">
                        <h1>Contact</h1>
                    </span>
                    <span id="footer-contact-text">
                        <a href="mailto:info@alliedfoundation.org">info@alliedfoundation.org</a>
                    </span>
                </div>

                <div id="footer-widget-4" class="footer-float footer-width">
                    <span id="footer-social">
                        <h1>Follow Us</h1>
                    </span>
                    <span id="footer-social-links">
                        <a href="https://www.facebook.com/thealliedfoundation" target="blank"><img src="<?php echo get_template_directory_uri(); ?>/img/facebook-icon.png" alt="facebook"></a>
<!--                         <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/twitter-icon.png" alt="twitter"></a>
                        <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/youtube-icon.png" alt="youtube"></a> -->
                    </span>
                </div>
                
            </footer>
			<!-- /footer -->

		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>

		<!-- analytics -->
		<script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
		</script>

	</body>
</html>
