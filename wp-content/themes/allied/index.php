<?php get_header(); ?>

	<section id="slider">
                <!-- Background image inserted here through css -->
            </section>

            <section id="projects">
                <div id="project-breasts" class="project-float project-width">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/allied-img-1.jpg">
                    <h1>Breastfeeding Support</h1>
                    <p>Allied Foundation supports breastfeeding in the community. The Allied Lactation Consultants are available to help guide every mother reach her personal breastfeeding goal.</p>
                </div>

                <div id="project-education" class="project-float project-width">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/allied-img-2.jpg">
                    <h1>Community Education</h1>
                    <p>Allied Foundation supports community-wide education. Free lecture series on topics including managing food allergies, ADHD, anxiety and more. A fall lecture series will be announced shortly.</p>
                </div>

                <div id="project-literacy" class="project-float project-width">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/allied-img-3.jpg">
                    <h1>Early Childhood Literacy</h1>
                    <p>From August 17-September 4, participating Allied Physicians Group offices will collect gently used books from their communities to be donated to The Book Fairies.</p>
                </div>
            </section>

            <div id="double-section">
                <section id="news">
                    <h1> In The News</h1> 
                    <div id="news-blog-featured">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/flu-season.jpg" style="width:250px; height:330px;">
                        <h2><a href="http://myemail.constantcontact.com/PHA-flu-vaccine-update.html?soid=1103980140268&aid=rGxPvZAMnz8" target="blank">Flu Vaccine Info Update</a></h2>
                    </div>
                    <div id="news-blog-2" class="news-blog-side" >
                        <img src="<?php echo get_template_directory_uri(); ?>/img/allied-thumb-1.jpg">
                        <h2><a href="http://www.newsday.com/lifestyle/family/pediatric-offices-collecting-books-for-underserved-kids-1.10754410?pts=221325" target="blank">Pediatric offices collecting books for underserved kids</a></h2>
                        <h3>New York Newsday</h3>
                        <p>From now through Sept. 4, participating Allied Physicians Group...</p>

                    </div>
                    <div id="news-blog-3" class="news-blog-side">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/allied-thumb-1.jpg">
                        <h2><a href="http://www.thebookfairies.org/" target="blank">The Book Fairies</a></h2>
                        <!-- <h3>Commack Pediatric Associates will now also be a drop-off site for the Allied Foundation...</h3> -->
                        <p>http://www.thebookfairies.org/</p>
                    </div>
                    <!-- <div id="news-blog-3" class="news-blog-side">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/allied-thumb-1.jpg">
                        <h2><a href="">Milestones in Philanthropy</a></h2>
                        <h3>July 1, 2015 - Anonymous</h3>
                        <p>The following timeline recounts some of the pivotal moments...</p>
                    </div> -->
                </section>

                <section id="calendar">
                    <h1>Upcoming Events</h1>
                    <img src="<?php the_field('upcoming_events_image'); ?>">
                </section>
            </div>

            <section id="mission">
                <p><?php the_field('mission_text'); ?></p>
            </section>
            
            <section id="sponsor">
                <div id="sponsor-text">
                    <h1>We wish to thank our sponsors</h1>
                </div>
                <div id="sponsor-logos">
                    <ul>
                        <li><img src="<?php echo get_template_directory_uri(); ?>/img/allied-sponsor-logo-1.png"></li>
                        <li><img src="<?php echo get_template_directory_uri(); ?>/img/allied-sponsor-logo-2.png"></li>
                        <li><img src="<?php echo get_template_directory_uri(); ?>/img/allied-sponsor-logo-3.png"></li>
                        <li><img src="<?php echo get_template_directory_uri(); ?>/img/allied-sponsor-logo-4.png"></li>
                    </ul>
                </div>
            </section>

            <section id="contact">
                <h1><a href="/contact-us">Get In Touch</a></h1>
                <a href="https://www.facebook.com/thealliedfoundation" target="blank">
                	<img class="social-logos" src="<?php echo get_template_directory_uri(); ?>/img/allied-social-logos.png">
                </a>
                <div class="contact-form ">
                    <?php
                    echo do_shortcode('[contact-form-7 id="15" title="Contact Us"]');
                    ?>
                </div>
            </section>

<?//php get_sidebar(); ?>

<?php get_footer(); ?>
