<?php get_header(); ?>

<section id="page-header">
                <h1 class="color-orange">Donate to Allied Foundation</h1>
                
            </section>

            <section id="donate">
                <div id="board-bio">
                    <div id="board-wrap-80">
                        <?php the_field('main_text'); ?>
                    </div>
                </div>
                
            </section>

            <div class="donate_options">
                <div>
                    <h2>Check</h2>
                    <img class="check" src="<?php echo get_template_directory_uri(); ?>/img/check.png">
                    <p>Please send checks payable to:<br><br>Allied Foundation<br>3 Huntington Quadrangle Suite 105s<br>Melville, NY 11747</p>
                </div>
                <div>
                    <h2>General Allied Fund</h2>
<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="LLXTY62MN92X2">
<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>
                                        <h2>Diaper Bank Donations ($24 per box)</h2>
<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="HC5LENFRY2Q8A">
<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>


                </div>

            </div>

            <div style="text-align:center;">
                <img src="<?php the_field('main_image'); ?>" style="width:75%;margin:0 auto;">
            </div>

            <section id="visit-allied">
                <a href="http://alliedphysiciansgroup.com" target="blank">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/allied-visit-site.jpg">
                </a>
            </section>

        

<?php get_footer(); ?>