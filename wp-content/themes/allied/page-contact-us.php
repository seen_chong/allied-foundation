<?php get_header(); ?>

    <section id="google-map-embed">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3021.4634013290906!2d-73.41837544869583!3d40.773825441665785!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89e82bcae680070f%3A0x26945768c590fc54!2s3+Huntington+Quadrangle+%23105s%2C+Melville%2C+NY+11747!5e0!3m2!1sen!2sus!4v1445377344862" width="100%" height="500px" frameborder="0" style="border:0" allowfullscreen></iframe>
    </section>

           
            <section id="page-header">
                <h1>Contact Us</h1>
            </section>

            <section id="contact" class="contact-height">
                <div id="contact-header">
                    <h1 class="follow-us-header">Follow Us On Social Media</h1>
                    <ul>
                        <li>
                            <a href="https://www.facebook.com/thealliedfoundation" target="blank"><img class="social-logos" src="<?php echo get_template_directory_uri(); ?>/img/facebook-icon.png"></a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/channel/UCS8synlMOxqO8nniJ8T99hQ" target="blank"><img class="social-logos" src="<?php echo get_template_directory_uri(); ?>/img/youtube-icon.png"></a>
                        </li>
                    </ul>
                </div>
                
                <div class="contact-form page-contact-form">

                    <?php
                    echo do_shortcode('[contact-form-7 id="15" title="Contact Us"]');
                    ?>

                </div>
            </section>


<?//php get_sidebar(); ?>

<?php get_footer(); ?>
